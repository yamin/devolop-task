export const getCategories = () => ({
  type: "GET_CATEGORIES",
});

export const getImages = (catId: number) => ({
  type: "GET_IMAGES",
  catId: catId,
});

export const loadMore = (page: number, catId: number) => ({
  type: "LOAD_MORE",
  page: page,
  catId: catId,
});
