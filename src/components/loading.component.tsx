import { connect, RootStateOrAny } from "react-redux";
import { InferProps } from "prop-types";
import styled, { keyframes } from "styled-components";

const LoadingWrapper = styled.div`
  left: 0;
  text-align: center;
  position: fixed;
  top: 0;
  width: 100vw;
  height: 100vh;
  background: #6f84a280;
  z-index: 100;
`;

const loading = keyframes`
  0% {
    transform: rotate(0);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }
  50% {
    transform: rotate(900deg);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }
  100% {
    transform: rotate(1800deg);
  }
`;

const LoadingIndicator = styled.div`
  display: inline-block;
  position: relative;
  width: 80px;
  height: 80px;
  &:after {
    content: " ";
    display: block;
    border-radius: 50%;
    width: 0;
    height: 0;
    margin: 8px;
    box-sizing: border-box;
    border: 32px solid #fff;
    border-color: #fff transparent #fff transparent;
    animation: ${loading} 1.2s infinite;
  }
`;

const LoadingContent = styled.div`
  position: absolute;
  top: 46%;
  left: 46%;
`;

const LoadingTitle = styled.h1`
  color: white;
`;

let Loading = ({ loading }: InferProps<typeof LoadingPropTypes>) => {
  return loading ? (
    <LoadingWrapper style={{ textAlign: "center" }}>
      <LoadingContent>
        <LoadingIndicator></LoadingIndicator>
        <LoadingTitle>LOADING</LoadingTitle>
      </LoadingContent>
    </LoadingWrapper>
  ) : null;
};

const LoadingPropTypes = {
  loading: Boolean,
};

const mapStateToProps = (state: RootStateOrAny) => ({ loading: state.loading });
Loading = connect(mapStateToProps, null)(Loading);
export default Loading;
