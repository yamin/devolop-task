const settings = {
    api: {
        categories: 'https://api.thecatapi.com/v1/categories',
        images: 'https://api.thecatapi.com/v1/images/search',
        url: 'https://docs.thecatapi.com/api-reference/images/images-search',
    },
    image:{
        limit: 10
    }
};


export default settings;