import { useEffect } from "react";
import { connect } from "react-redux";
import { getCategories } from "../actions";
import Sidebar from "./sidebar/siderbar.component";
import Loading from "./loading.component";
import GridComponent from "./content/grid.component";
import styled from "styled-components";

const Wrapper = styled.div`
  height: 100%;
  margin: 0;
  display: flex;
`;

const SidebarWrapper = styled.div`
  width: 300px;
  background: #1c72fd;
  color: #bbbbbb;
  margin: 0;
  padding: 0;
  position: fixed;
  height: 100%;
  overflow: auto;
`;

const Content = styled.div`
  margin-left: 300px;
`;

const MainNotConnected = ({ getCategories }: {getCategories: Function} ) => {
  useEffect(() => {
    getCategories();
    return () => {};
  }, [getCategories]);
  return (
    <div>
      <Loading />
      <Wrapper>
        <SidebarWrapper>
          <Sidebar />
        </SidebarWrapper>
        <Content>
          <GridComponent />
        </Content>
      </Wrapper>
    </div>
  );
};

const mapDispatchToProps = {
  getCategories: getCategories,
};

const Main = connect(null, mapDispatchToProps)(MainNotConnected);

export default Main;
