# Develon React task
#### Developed by Yamin Tashakkori. March 15 2021

This is a simple React application that fetches images of cats based on some categories. the API is provided by [the cat API](https://docs.thecatapi.com/api-reference/images/images-search).


## Pull code

Pull the code using 
```bash
git clone git@github.com:YaminT/develop-task.git
```
Or
```bash
https://github.com/YaminT/develop-task.git
```
## Install the dependencies
Install the dependencies using:
```bash
npm install --production
```
## Run
Run application in development mode using:
```bash
npm start
```
Or get a build and run the build separately using:
```bash
npm run build
```

## Notes
 - this app is using Redux, Redux Saga for state management
 - The React router and lazy loading are not used.
 - The application is written in Typescript.
 - vscode launch file is included
 - StyledComponent is used for styling
