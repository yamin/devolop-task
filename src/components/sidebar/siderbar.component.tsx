import { connect } from "react-redux";
import styled from "styled-components";
import { StateInterface } from "../../types/action.type";
import { Category } from "../../types/category.interface";
import CategoryComponent from "./category.component";

const SidebarWrapper = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 14px;
  color: white;
`;

const SideBarTitle = styled.div`
  display: flex;
  /* font-size: em; */
  align-items: center;
  justify-content: start;
  width: 100%;

`;

const SideBarText = styled.div`
  margin: 5px ;
`;

const SidebarNotConnected = ({ categories,selectedCategory }: any) => {
  if (categories) {
    const renderedCategories = categories.map((cat: Category) => (
      <CategoryComponent category={cat} key={cat.id} />
    ));
    return (
      <SidebarWrapper>
        <SideBarTitle>
          <span className="material-icons">category</span><SideBarText>Categories</SideBarText>
        </SideBarTitle>
        {renderedCategories}
      </SidebarWrapper>
    );
  }
  return <div>Sidebar</div>;
};

const mapStateToProps = (state: StateInterface) => ({
  categories: state.categories,
  selectedCategory: state.selectedCategory
});

const Sidebar = connect(mapStateToProps, null)(SidebarNotConnected);

export default Sidebar;
