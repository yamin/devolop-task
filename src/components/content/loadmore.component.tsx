import { Dispatch } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { loadMore } from "../../actions";
import { ActionType, StateInterface } from "../../types/action.type";
import { WideButton } from "../../styles";

const LoadMoreWrapper = styled.div`
  padding: 20px 0;
`;

export const BtnText = styled.span`
  display: block;
  padding: 22px 0;
  text-align: center;
  line-height: 1em;
  margin: 0 auto;
`;

type OwnProps = {
  receivedImagesCount: number;
};

let LoadMoreNotConnected = ({
  receivedImagesCount,
  catId,
  page,
  nextPageIsAvailable,
  loadMore,
}: any) => {
  if (nextPageIsAvailable) {
    return (
      <LoadMoreWrapper>
        <WideButton onClick={() => loadMoreAction(loadMore, page, catId)}>
          <BtnText>Load more</BtnText>
        </WideButton>
      </LoadMoreWrapper>
    );
  }
  return (
    <button disabled>
      <span>Nothing else to show</span>
    </button>
  );
};

const loadMoreAction = (loadMore: Function, page: number, catId: number) => {
  loadMore(++page, catId);
};

const mapDispatchToProps = (dispatch: Dispatch<ActionType>) => ({
  loadMore: (page: number, catId: number) => dispatch(loadMore(page, catId)),
});

const mapStateToProps = (state: StateInterface, ownProps: OwnProps) => ({
  catId: state.selectedCategory,
  page: state.page,
  nextPageIsAvailable: state.nextPageIsAvailable,
});

const LoadMore = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoadMoreNotConnected);

export default LoadMore;
