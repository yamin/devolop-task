import styled from "styled-components";

export const Button = styled.button`
  border: none;
  background: transparent;
  color: white;
  font-size: 0.9em;
  width: 100%;
  display: flex;
  cursor: pointer;
  border-radius: 5px;
  padding: 10px;
  text-align: start;
  align-items: center;

  &:hover {
    background: #00000040;
  }

  &:focus {
    outline: none;
  }
`;

export const WideButton = styled(Button)`
  background-color: #3e3e3e;
  text-align: center;
  width: 300px;
  margin: 0 auto;
`;