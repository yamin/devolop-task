import { connect } from "react-redux";
import styled from "styled-components";
import { StateInterface } from "../../types/action.type";
import CatImage from "./catimages.component";
import LoadMore from "./loadmore.component";

const Centered = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
`;

const ImageWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

/* Received ids are not unique. the index is added to ensure uniqueness*/
const GridComponentNotConnected = ({ images }: any) => {
  if (images && images.length > 0) {
    const renderedImages = images.map(
      (img: Record<string, any>, index: number) => (
        <CatImage key={img.id + index} image={img} />
      )
    );
    return (
      <>
        <ImageWrapper>{renderedImages}</ImageWrapper>
        <LoadMore receivedImagesCount={images.length} />
      </>
    );
  }
  return <Centered>Nothing to show</Centered>;
};

const mapStateToProps = (state: StateInterface) => ({
  images: state.images,
});

const GridComponent = connect(mapStateToProps)(GridComponentNotConnected);

export default GridComponent;
