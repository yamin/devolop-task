import styled from "styled-components";

const ImageItem = styled.div`
  margin: 10px;
  border: 1px solid #cecece;
  background-color: #ececec;
  border-radius: 10px;
  padding: 10px;
  width: 300px;
  height: 300px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Image = styled.img`
  max-width: 100%;
  max-height: 100%;
`;

let CatImage = ({ image }: any) => (
  <ImageItem>
    <Image src={image.url} />
  </ImageItem>
);

export default CatImage;
