export interface ReduxAction {
    type: string;
    data: any;
}

export interface ActionType {
    type: string;
}

export interface ImageActionType {
    type: string,
    catId: number 
}

export interface LoadMoreActionType {
    type: string;
    page: number;
    catId: number;
}

export interface CategoryInterface{
    id: number;
    name: string;
}

export interface ImageInterface {
    breeds: [];
    categories: CategoryInterface[];
    height: number;
    id: string;
    url: string;
    width: number;
}

export interface StateInterface {
    categories: CategoryInterface[];
    loading: Boolean;
    selectedCategory?: number;
    images: ImageInterface[];
    page: number;
    nextPageIsAvailable: Boolean
}

export interface RequestParamsInterface {
    page: number;
    limit: number;
    category_ids: number;
}