import { put, takeLatest, all } from "redux-saga/effects";
import configs from "../configs";
import { ImageActionType, LoadMoreActionType, RequestParamsInterface, ImageInterface, CategoryInterface } from "../types/action.type";
import { queryStringGenerator } from "../tools";

function* fetchCategories(): IterableIterator<any> {
  const parsedResponse: CategoryInterface[] | undefined = yield fetch(
    configs.api.categories
  ).then((response) => response.json());
  if (parsedResponse) {
    yield put({ type: "CATEGORIES_DOWNLOADED", data: parsedResponse });
  } else {
    yield put({ type: "ERROR", json: {} });
  }
}

function* actionWatcher() {
  yield takeLatest("GET_CATEGORIES", fetchCategories);
}

function* imageSaga() {
  yield takeLatest("GET_IMAGES", fetchSelectedCat);
}

function* loadMoreSaga() {
  yield takeLatest("LOAD_MORE", loadMoreImage);
}

function* loadMoreImage(payload: LoadMoreActionType): IterableIterator<any> {
  const params: Record<string, any> = {
    page: payload.page,
    limit: configs.image.limit,
    category_ids: payload.catId,
  };
  const query = queryStringGenerator(params);
  const parsedResponse: ImageInterface[] | undefined = yield fetch(
    configs.api.images + "?" + query
  ).then((response) => response.json());
  if (parsedResponse) {
    yield put({ type: "IMAGES_ADDED", data: parsedResponse });
  } else {
    yield put({ type: "ERROR_ADDING_IMAGES", data: {} });
  }
}

function* fetchSelectedCat(payload: ImageActionType): IterableIterator<any> {
  const params: RequestParamsInterface = {
    page: 1,
    limit: configs.image.limit || 10,
    category_ids: payload.catId,
  };
  const query = queryStringGenerator(params);
  const parsedResponse: ImageInterface[] | undefined = yield fetch(
    configs.api.images + "?" + query
  ).then((response) => response.json());
  if (parsedResponse) {
    yield put({ type: "IMAGES_DOWNLOADED", data: parsedResponse });
  } else {
    yield put({ type: "ERROR_DOWNLOADING_IMAGES", data: {} });
  }
}

export default function* rootSaga() {
  yield all([actionWatcher(), imageSaga(), loadMoreSaga()]);
}
