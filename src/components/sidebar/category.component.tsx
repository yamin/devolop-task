import { Category } from "../../types/category.interface";
import { Dispatch } from "react";
import { getImages } from "../../actions";
import { connect } from "react-redux";
import { ActionType } from "../../types/action.type";
import styled from "styled-components";
import { Button } from "../../styles";

const CategoryText = styled.div`
  margin-left: 10px;
`;

const CategoryComponentNotConnected = ({ category, getImages }: any) => {
  return (
    <Button onClick={() => selectCategory(category, getImages)}>
      <CategoryText>{category.name}</CategoryText>
    </Button>
  );
};

const selectCategory = (category: Category, getImages: Function) => {
  getImages(category.id);
};

const mapDispatchToProps = (dispatch: Dispatch<ActionType>) => ({
  getImages: (catId: number) => dispatch(getImages(catId)),
});

const CategoryComponent = connect(null, mapDispatchToProps)(CategoryComponentNotConnected);

export default CategoryComponent;
