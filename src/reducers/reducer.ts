import configs from "../configs";
import { checkIfNextPageIsAvailable } from "../tools";
import { StateInterface } from "../types/action.type";

const reducer = (
  state: StateInterface = {loading: false,images: [],page: 0, categories: [],nextPageIsAvailable: false},
  action: Record<string, any>
) => {

  switch (action.type) {
    case "LOAD_MORE":
      return { ...state, loading: true };
    case "IMAGES_ADDED":
      const images = state.images.concat(action.data);
      const page = ++state.page;
      return {
        ...state,
        loading: false,
        images: images,
        page: page,
        nextPageIsAvailable: checkIfNextPageIsAvailable(
          action.data.length,
          configs.image.limit
        ),
      };
    case "IMAGES_DOWNLOADED":
      return {
        ...state,
        loading: false,
        images: action.data,
        page: 1,
        nextPageIsAvailable: true,
      };
    case "GET_IMAGES":
      return { ...state, loading: true, selectedCategory: action.catId };
    case "GET_CATEGORIES":
      return { ...state, loading: true };
    case "CATEGORIES_DOWNLOADED":
      return { ...state, loading: false, categories: action.data };
    default:
      return state;
  }
};

export default reducer;
