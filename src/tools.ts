export function queryStringGenerator(obj: Record<string, any>) {
  return Object.keys(obj).map(key => key + '=' + obj[key]).join('&');
}

export function checkIfNextPageIsAvailable(length : number, limit: number){
  return length === limit;
}